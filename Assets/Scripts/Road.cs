﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Road : MonoBehaviour {

	private IEnumerator coroutine;
	private float waitSeconds = 1f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

	}

	void OnTriggerExit(Collider other) {

		print ("Helllllooooo");
		if (other.CompareTag ("Player")) {
			coroutine = Recycle ();
			StartCoroutine (coroutine);
		}			
	}

	private IEnumerator Recycle() {
		yield return new WaitForSeconds(0.5f);
		Debug.Log ("Before Invisibel");
		gameObject.SetActive (false);
		RoadManager.Instance.RecycleRoad (gameObject);
		Debug.Log ("Invisibel");
		RoadManager.Instance.SpawnRoad ();
	}
}
