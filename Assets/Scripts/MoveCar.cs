﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class MoveCar : MonoBehaviour {

	public float TurnSpeed = 100f;
	public float MoveSpeed = 100f;

	private Rigidbody Body = null;

	void Awake() {
		Body = GetComponent<Rigidbody> ();
	}

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		Body.velocity = transform.forward * MoveSpeed;
		float Horizontal = CrossPlatformInputManager.GetAxis ("Horizontal");
		Body.AddForce (transform.right * TurnSpeed * Horizontal);


		//transform.position += transform.forward * MoveSpeed * Time.deltaTime;
		//transform.position += transform.right * MoveSpeed * Horizontal * Time.deltaTime;	
	}
}
