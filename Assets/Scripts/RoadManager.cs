﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadManager : MonoBehaviour {

	public static RoadManager Instance = null;

	public GameObject RoadPrefab = null;

	public GameObject CurrentRoad = null;

	private Queue<GameObject> roads = new Queue<GameObject> ();

	void Awake() {

		if (Instance == null) {
			Instance = this;	
		} 
		else if (Instance != null) {
			Destroy (gameObject);
		}
	}

	// Use this for initialization
	void Start () {

		createRoads (3);

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void createRoads(int count) {

		for (int i=0; i< count; i++) {
			GameObject road = Instantiate (RoadPrefab) as GameObject;
			road.SetActive (false);
			roads.Enqueue (road);
			SpawnRoad ();
		}
	}

	public void SpawnRoad() {
		GameObject road = roads.Dequeue();
		road.SetActive (true);
		print ("Spawning new");
		print (CurrentRoad.transform.position);
		road.transform.position = CurrentRoad.transform.GetChild(0).transform.position;
		CurrentRoad = road;

	}

	public void RecycleRoad(GameObject road) {
		road.SetActive (false);
		roads.Enqueue (road);
	}
}
